from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import (CandidaturaViewSet, PessoaViewSet,
                    RankingCandidaturaViewSet, VagaViewSet)

app_name = 'core'

router = DefaultRouter()

router.register(r'vagas', VagaViewSet, 'vagas')
router.register(r'vagas/(?P<id_vaga>[\d]+)/candidaturas/ranking', RankingCandidaturaViewSet, 'rank')
router.register(r'pessoas', PessoaViewSet, 'pessoas')
router.register(r'candidaturas', CandidaturaViewSet, 'candidaturas')

urlpatterns = [
    path(r'', include(router.urls)),
]
