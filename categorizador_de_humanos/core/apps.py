from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'categorizador_de_humanos.core'
