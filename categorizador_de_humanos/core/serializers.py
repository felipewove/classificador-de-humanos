from rest_framework import serializers

from .models import Candidatura, Pessoa, Vaga


class VagaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vaga
        fields = ('__all__')


class PessoaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pessoa
        fields = ('__all__')


class CandidaturaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Candidatura
        fields = ('__all__')


class RankingCandidaturaSerializer(serializers.ModelSerializer):
    nome = serializers.CharField(source='id_pessoa.nome')
    profissao = serializers.CharField(source='id_pessoa.profissao')
    localizacao = serializers.CharField(source='id_pessoa.localizacao')
    nivel = serializers.IntegerField(source='id_pessoa.nivel')

    class Meta:
        model = Candidatura
        fields = ('__all__')
