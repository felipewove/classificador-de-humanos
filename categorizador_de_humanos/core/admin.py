from django.contrib import admin

from .models import Candidatura, Pessoa, Vaga

admin.site.register(Candidatura)
admin.site.register(Pessoa)
admin.site.register(Vaga)
