from django.shortcuts import render
from rest_framework import viewsets

from .models import Candidatura, Pessoa, Vaga
from .serializers import (CandidaturaSerializer, PessoaSerializer,
                          RankingCandidaturaSerializer, VagaSerializer)


class VagaViewSet(viewsets.ModelViewSet):
    queryset = Vaga.objects.all()
    serializer_class = VagaSerializer
    lookup_field = 'id'


class PessoaViewSet(viewsets.ModelViewSet):
    queryset = Pessoa.objects.all()
    serializer_class = PessoaSerializer
    lookup_field = 'id'


class CandidaturaViewSet(viewsets.ModelViewSet):
    queryset = Candidatura.objects.all()
    serializer_class = CandidaturaSerializer
    lookup_field = 'id'


class RankingCandidaturaViewSet(viewsets.ModelViewSet):
    serializer_class = RankingCandidaturaSerializer

    def get_queryset(self):
        id_vaga = self.kwargs['id_vaga']
        return Candidatura.objects.filter(id_vaga=id_vaga).order_by('-score')
