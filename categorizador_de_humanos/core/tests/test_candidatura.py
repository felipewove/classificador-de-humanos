from categorizador_de_humanos.core.models import Candidatura, Pessoa, Vaga
from django.core.exceptions import ValidationError
from django.test import TestCase


class TestCandidatura(TestCase):
    def setUp(self):
        self.pessoa_1 = Pessoa.objects.create(
            nome='John Doe',
            profissao='Engenheiro de Software',
            localizacao='C',
            nivel=2
        )
        self.pessoa_2 = Pessoa.objects.create(
            nome="Mary Jane",
            profissao="Engenheira de Software",
            localizacao="A",
            nivel=4,
        )
        self.vaga_1 = Vaga.objects.create(
            empresa="Teste",
            titulo="Vaga teste",
            descricao="Criar os mais diferentes tipos de teste",
            localizacao="A",
            nivel=3
        )

    def test_create_candidatura(self):
        candidatura = Candidatura.objects.create(
            id_vaga=self.vaga_1,
            id_pessoa=self.pessoa_1
        )
        self.assertEqual(candidatura.score, 62)

    def test_dont_let_create_another_candidatura_for_same_person(self):
        Candidatura.objects.create(
            id_vaga=self.vaga_1,
            id_pessoa=self.pessoa_1
        )
        self.assertEqual(Candidatura.objects.all().count(), 1)
        with self.assertRaises(ValidationError):
            Candidatura.objects.create(
                id_vaga=self.vaga_1,
                id_pessoa=self.pessoa_1
            )
        self.assertEqual(Candidatura.objects.all().count(), 1)

    def test_when_candidate_updates_localitzation(self):
        candidatura = Candidatura.objects.create(
            id_vaga=self.vaga_1,
            id_pessoa=self.pessoa_1
        )
        self.assertEqual(candidatura.score, 62)

        self.pessoa_1.localizacao = "F"
        self.pessoa_1.save()
        candidatura.refresh_from_db()
        self.assertEqual(candidatura.score, 50)
