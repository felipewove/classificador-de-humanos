from categorizador_de_humanos.core.utils import (calculate_coefficient_experience,
                                                 calculate_distance,
                                                 calculate_distance_from_cache,
                                                 calculate_score)
from django.test import TestCase


class TestCalculateCoefficientExperience(TestCase):
    def test_calculate_coefficient_experience_equal(self):
        level_opportunity = 3
        level_person = 3
        self.assertEqual(
            calculate_coefficient_experience(
                level_opportunity, level_person
            ),
            100
        )

    def test_calculate_coefficient_experience_opportunity_greater_person(self):
        level_opportunity = 5
        level_person = 3
        self.assertEqual(
            calculate_coefficient_experience(
                level_opportunity, level_person
            ),
            50
        )

    def test_calculate_coefficient_experience_opportunity_lesser_person(self):
        level_opportunity = 3
        level_person = 5
        self.assertEqual(
            calculate_coefficient_experience(
                level_opportunity, level_person
            ),
            50
        )

    def test_calculate_coefficient_experience_is_zero(self):
        level_opportunity = 1
        level_person = 5
        self.assertEqual(
            calculate_coefficient_experience(
                level_opportunity, level_person
            ),
            0
        )


class TestCalculateScore(TestCase):
    def test_calculate_score_excelent_fit(self):
        level_opportunity = 3
        level_person = 3
        distance = 100
        self.assertEqual(
            calculate_score(
                level_opportunity, level_person, distance
            ),
            100
        )

    def test_calculate_score_half_fit_by_distance(self):
        level_opportunity = 3
        level_person = 3
        distance = 50
        self.assertEqual(
            calculate_score(
                level_opportunity, level_person, distance
            ),
            75
        )

    def test_calculate_score_half_fit_distance_and_level(self):
        level_opportunity = 5
        level_person = 3
        distance = 50
        self.assertEqual(
            calculate_score(
                level_opportunity, level_person, distance
            ),
            50
        )

    def test_calculate_score_poor_fit_by_distance(self):
        level_opportunity = 3
        level_person = 3
        distance = 25
        self.assertEqual(
            calculate_score(
                level_opportunity, level_person, distance
            ),
            62
        )

    def test_calculate_score_poor_fit_by_distance_and_level(self):
        level_opportunity = 3
        level_person = 5
        distance = 25
        self.assertEqual(
            calculate_score(
                level_opportunity, level_person, distance
            ),
            37
        )

    def test_calculate_score_bad_fit_by_distance(self):
        level_opportunity = 3
        level_person = 3
        distance = 0
        self.assertEqual(
            calculate_score(
                level_opportunity, level_person, distance
            ),
            50
        )

    def test_calculate_score_bad_fit_by_distance_and_level(self):
        level_opportunity = 5
        level_person = 1
        distance = 0
        self.assertEqual(
            calculate_score(
                level_opportunity, level_person, distance
            ),
            0
        )


class TestCalculateScoreFromCache(TestCase):
    def test_calculate_distance_from_cache_A_to_F(self):
        localization_opportunity = 'A'
        localization_person = 'F'
        self.assertEqual(
            calculate_distance_from_cache(
                localization_opportunity, localization_person
            ),
            16
        )

    def test_calculate_distance_from_cache_A_to_B(self):
        localization_opportunity = 'A'
        localization_person = 'B'
        self.assertEqual(
            calculate_distance_from_cache(
                localization_opportunity, localization_person
            ),
            5
        )

    def test_calculate_distance_from_cache_A_to_C(self):
        localization_opportunity = 'A'
        localization_person = 'C'
        self.assertEqual(
            calculate_distance_from_cache(
                localization_opportunity, localization_person
            ),
            12
        )


# class TestCalculateScore(TestCase):
#     def test_calculate_distance_A_to_F(self):
#         localization_opportunity = 'A'
#         localization_person = 'F'
#         self.assertEqual(
#             calculate_distance(
#                 localization_opportunity, localization_person
#             ),
#             16
#         )

#     def test_calculate_distance_A_to_B(self):
#         localization_opportunity = 'A'
#         localization_person = 'B'
#         self.assertEqual(
#             calculate_distance(
#                 localization_opportunity, localization_person
#             ),
#             5
#         )

#     def test_calculate_distance_A_to_C(self):
#         localization_opportunity = 'A'
#         localization_person = 'C'
#         self.assertEqual(
#             calculate_distance(
#                 localization_opportunity, localization_person
#             ),
#             12
#         )
