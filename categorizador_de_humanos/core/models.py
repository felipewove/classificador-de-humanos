from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from .constants import NIVEL_EXPERIENCIA
from .utils import calculate_distance_percent, calculate_score


class Vaga(models.Model):
    empresa = models.CharField(max_length=255)
    titulo = models.CharField(max_length=255)
    descricao = models.CharField(max_length=255)
    localizacao = models.CharField(max_length=255)
    nivel = models.PositiveIntegerField(choices=NIVEL_EXPERIENCIA, default=1)


class Pessoa(models.Model):
    nome = models.CharField(max_length=255)
    profissao = models.CharField(max_length=255)
    localizacao = models.CharField(max_length=255)
    nivel = models.PositiveIntegerField(choices=NIVEL_EXPERIENCIA, default=1)


class Candidatura(models.Model):
    id_vaga = models.ForeignKey('Vaga', on_delete=models.CASCADE)
    id_pessoa = models.ForeignKey('Pessoa', on_delete=models.CASCADE)
    score = models.PositiveIntegerField(default=0)

    class Meta:
        unique_together = (('id_vaga', 'id_pessoa'),)

    def update_score(self):
        distance_percent = calculate_distance_percent(
            localization_opportunity=self.id_vaga.localizacao,
            localization_person=self.id_pessoa.localizacao
        )
        self.score = calculate_score(
            level_opportunity=self.id_vaga.nivel,
            level_person=self.id_pessoa.nivel,
            distance=distance_percent
        )

    def save(self, *args, **kwargs):
        self.update_score()
        self.full_clean()
        return super(Candidatura, self).save(*args, **kwargs)


### Signals ###


@receiver(post_save, sender=Pessoa)
def update_applications(sender, instance, **kwargs):
    applications = Candidatura.objects.filter(id_pessoa=instance.id)
    for application in applications:
        application.save()
