import math

import environ

from .constants import CACHED_MAP_DISTANCE, MAP


class RangeDict(dict):
    '''
    Solution from
    https://stackoverflow.com/questions/39358092/range-as-dictionary-key-in-python

    Uses a dict with the range beeing its key
    '''
    def __getitem__(self, item):
        if type(item) != range:
            for key in self:
                if item in key:
                    return self[key]
        else:
            return super().__getitem__(item)

# when using this dict on constants, a loop of cycle import happend when importinf RangeDict there
TABLE_DISTANCE_PERCENT = RangeDict({
    range(0,5): 100,
    range(6,10): 75,
    range(11,15): 50,
    range(16,20): 25,
    range(20,100): 0
})

def is_map_cached():
    env = environ.Env()
    return env.bool('MAP_CACHED', default=False)

def calculate_score(level_opportunity, level_person, distance):
    level = calculate_coefficient_experience(level_opportunity, level_person)
    return math.floor((level + distance) / 2)

def calculate_coefficient_experience(level_opportunity, level_person):
    return 100 - 25 * math.fabs(level_opportunity - level_person)

def calculate_distance(localization_opportunity, localization_person, visited=list(), current_distance=0):
    pass

def calculate_distance_from_cache(localization_opportunity, localization_person):
    return CACHED_MAP_DISTANCE[localization_opportunity][localization_person]

def calculate_distance_percent(localization_opportunity, localization_person):
    distance = -1
    if is_map_cached():
        distance = calculate_distance_from_cache(localization_opportunity, localization_person)
    else:
        distance = calculate_distance(localization_opportunity, localization_person)
    return distance_table_percent(distance)

def distance_table_percent(distance):
    return TABLE_DISTANCE_PERCENT[distance]
