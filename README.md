# Classificador de Humanos

Classificador de Humanos candidatos a uma vaga, de acordo com nível de experiência.

Projeto em Python com as framework Django 2.0 e Django Rest Framework. Utilizando Docker para controlar o ambiente.

Foi utilizado o [CookieCutter](https://github.com/pydanny/cookiecutter-django) para iniciliazar.

## Subindo o projeto

Use o comando para subir uma instância:

```bash
docker-compose -f local.yml up --build
```

Acesse http://localhost:8000/ para acessar a aplicação. __Não está implementado essa interface padrão do Django__.

Acesse http://localhost:8000/v1/ para acessar a API v1 da aplicação.

## Rodando os testes

Utilze o PyTest para executar os testes unitários desse projeto.

```bash
docker-compose -f local.yml run django pytest
```

## Endpoints

### /vagas

#### GET

Lista todas as vagas da aplicação, sem paginação.

#### POST

Cria uma vaga e retorna o objeto criado.

```json
{
"empresa": "Teste",
"titulo": "Vaga teste",
"descricao": "Criar os mais diferentes tipos de teste",
"localizacao": "A",
"nivel": 3
}
```

### /pessoas

#### GET

Lista todas os candidatos da aplicação, sem paginação.

#### POST

Cria uma vaga e retorna o objeto criado.

```json
{
"nome": "John Doe",
"profissao": "Engenheiro de Software",
"localizacao": "C",
"nivel": 2
}
```

### /candidaturas

#### GET

Lista todas as candidaturas da aplicação, sem paginação.


#### POST

Cria uma candidatura e retorna o objeto criado, calculado o score.

```json
{
"id_vaga": 1,
"id_pessoa": 2
}
```

### /vagas/<vaga_id>/candidaturas/ranking

#### GET

Lista todas as candidaturas de uma vaga, sem paginação. Ordenado pelo score.

Exibe dados do candidato (nome, profissão, localização e nível) acrescido do score da candidatura.

## Decisões

### Atualização de score pelo Candidato

Decidi que caso um candidato atualize sua localização ou até mesmo atualize seu nível, é necessário atualizar o score dele em todas as vagas participantes.

### Mapa

Não foi implementado Djisktra para encontrar o menor caminho entre duas localidades.

Decide pegar distância de um ponto a todos os pontos do grafo e, então, criar um cache do resultado na aplicação.

Para isso, foi necessário usar a variável de ambiente `MAP_CACHED` para definir qual mapa será utilizado.

No arquivo `local.yml`, está setado `MAP_CACHED=True`. Caso altere para `MAP_CACHED=False`, ocorrerá um erro de execução ao tentar atualizar um candidato ou criar uma candidatura.

### Tabela D

Essa tabela é responsável por atribuir uma porcentagem dada a distância do Candidadto para a Vaga.

Utilizando uma [solução de RangeDict](https://stackoverflow.com/questions/39358092/range-as-dictionary-key-in-python), tenho acesso direto a porcentagem dado a distância calculada. Sem precisa de case-switch e/ou instânciando a cada iteração um conjunto de range

### Omissão de testes dos modelos Pessoa e Vaga

Por serem modelos simples e não ter regras de negócios no enunciado, todos os campos são texto de até 255 caracteres, estão cobertos pelo Django, não precisando realizar uma dupla-verificação
